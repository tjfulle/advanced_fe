import numpy as np

import fetk.dload
from .LND2 import L1D2
from .base import element, PLANE, OFF, ON
from fetk.util.geometry import edge_normal


class C2D3(element):
    signature = (PLANE, 2, ON, ON, OFF, OFF, OFF, OFF, OFF, OFF, 1, 3)
    properties = {"thickness": {"aliases": ["t", "h"], "default": 1.0}}

    def init(self):
        self.edge = L1D2(A=1)

    def area(self, xc):
        """Returns the area of the triangle

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
            xc[:, 0] (len=3) are the x coordinates, in the order shown above
            xc[:, 1] (len=3) are the y coordinates

        Returns
        -------
        a : float
            The area

        """
        x1, x2, x3 = xc[:3, 0]
        y1, y2, y3 = xc[:3, 1]
        a = (x1 * y2 - x2 * y1) - (x1 * y3 - x3 * y1) + (x2 * y3 - x3 * y2)
        return a / 2.0

    def volume(self, xc):
        """Volume of the quadrilateral, `v = a(xc) * thickness"""
        return self.thickness * self.area(xc)

    @property
    def edges(self):
        return [[0, 1], [1, 2], [2, 0]]

    def jacobian(self, xc):
        """The Jacobian of the transformation from phyiscal to natural coordinates

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
            xc[:, 0] (len=3) are the x coordinates, in the order shown above
            xc[:, 1] (len=3) are the y coordinates

        Returns
        -------
        J : float
            J = det(Jm), where Jm is the Jacobian matrix Jm = dx/ds

        """
        return 2 * self.area(xc)

    def shape(self, xc, p):
        """Shape functions in the triangle (natural) coordinates

        Parameters
        ----------
        xg : ndarray
            si = xg[i] is the coordinate of the ith Gauss point

        """
        x, y = self.isop_map(xc, p)
        x1, x2, x3 = xc[:3, 0]
        y1, y2, y3 = xc[:3, 1]
        N1 = x2 * y3 - x3 * y2 + (y2 - y3) * x + (x3 - x2) * y
        N2 = x3 * y1 - x1 * y3 + (y3 - y1) * x + (x1 - x3) * y
        N3 = x1 * y2 - x2 * y1 + (y1 - y2) * x + (x2 - x1) * y
        Ae = self.area(xc)
        return np.array([N1, N2, N3]) / 2 / Ae

    def shapegrad(self, xc):
        """Derivatives of shape functions, wrt to the physical coordinates

        Parameters
        ----------
        xg : ndarray
            si = xg[i] is the coordinate of the ith Gauss point

        """
        Ae = self.area(xc)
        x1, x2, x3 = xc[:3, 0]
        y1, y2, y3 = xc[:3, 1]
        dN = np.array([[y2 - y3, y3 - y1, y1 - y2], [x3 - x2, x1 - x3, x2 - x1]])
        return dN / 2.0 / Ae

    @property
    def gauss_points(self):
        return np.array([[1.0, 1.0], [4.0, 1.0], [1.0, 4.0]]) / 6.0

    @property
    def gauss_weights(self):
        return np.ones(3) / 3.0

    def isop_map(self, xc, p):
        """Map the point `p` in natural coordinates to physical coordinates

        Paratemeters
        ------------
        xc : ndarray
            Nodal coordinates
            xc[:, 0] (len=3) are the x coordinates, in the order shown above
            xc[:, 1] (len=3) are the y coordinates
        p : list of float
            s, t = p

        """
        s, t = p
        x1, x2, x3 = xc[:3, 0]
        y1, y2, y3 = xc[:3, 1]
        x = x1 * s + x2 * t + x3 * (1 - s - t)
        y = y1 * s + y2 * t + y3 * (1 - s - t)
        return x, y

    def bmatrix(self, xc):
        """Compute the element B matrix

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
        xg : ndarray
            si = xg[i] is the coordinate of the ith Gauss point

        Returns
        -------
        B : ndarray
            The B matrix, or matrix containing derivatives of the shape function
            with respect to the physical coordinates

        """
        dNdx = self.shapegrad(xc)
        num_node = self.num_nodes
        num_dof_per_node = self.num_dof_per_node
        B = np.zeros((3, num_dof_per_node * num_node))
        B[0, 0::num_dof_per_node] = B[2, 1::num_dof_per_node] = dNdx[0, :]
        B[1, 1::num_dof_per_node] = B[2, 0::num_dof_per_node] = dNdx[1, :]
        return B

    def pmatrix(self, xc, p):
        """Compute the element P matrix

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
            xc[:, 0] (len=3) are the x coordinates, in the order shown above
            xc[:, 1] (len=3) are the y coordinates
        p : list of float
            s, t = p

        Returns
        -------
        P : ndarray
            The P matrix, or matrix containing the shape functions.

        Notes
        -----
        The P matrix is setup such that values of an element constant field `f`
        can be interpolated to a point as:

            x = dot(P.T, f)

        """
        num_nodes = self.num_nodes
        num_dof_per_node = self.num_dof_per_node
        N = self.shape(xc, p)
        P = np.zeros((num_dof_per_node, num_nodes * num_dof_per_node))
        P[0, 0::num_dof_per_node] = N
        P[1, 1::num_dof_per_node] = N
        return P

    def stiffness(self, xc, material, *args):
        """Compute the element stiffness

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates

        Returns
        -------
        ke : ndarray
            The element stiffess
            ke = integrate(B.T, C, B)

        """
        Ae = self.area(xc)
        Be = self.bmatrix(xc)
        he = self.thickness
        ke = np.zeros((6, 6))
        p1 = 3
        for p in range(p1):
            wg = self.gauss_weights[p]
            C = material.stiffness(xc, ndir=2, nshr=1)
            ke += Ae * he * wg * np.dot(np.dot(Be.T, C), Be)
        return ke

    def force(self, xc, dload):
        """Compute the element force

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates

        Returns
        -------
        fe : ndarray
            The element force
            fe = integrate(transpose(P).f)

        """
        num_nodes = self.num_nodes
        num_dof_per_node = self.num_dof_per_node
        num_dof = num_nodes * num_dof_per_node
        fe = np.zeros(num_dof)
        if dload is None:
            return fe
        for (tag, edge_no, f) in dload.items():
            if tag == fetk.dload.body_force:
                fe += self.body_force(xc, f[:num_dof_per_node])
            elif tag == fetk.dload.surface_force:
                fe += self.surface_force(xc, edge_no, f[:num_dof_per_node])
            elif tag == fetk.dload.pressure:
                edge = self.edges[edge_no]
                n = edge_normal(xc[edge])
                fe += self.surface_force(xc, edge_no, f[0] * n)
        return fe

    def body_force(self, xc, f):
        """Compute the element force

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
        f : array_like
            The array of distributed loads

        Returns
        -------
        fe : ndarray
            The element force
            fe = integrate(transpose(P).f)

        """
        num_nodes = self.num_nodes
        num_dof_per_node = self.num_dof_per_node
        num_dof = num_nodes * num_dof_per_node
        he = self.thickness
        Ae = self.area(xc)
        fe = np.zeros(num_dof)
        for p in range(len(self.gauss_points)):
            xg = self.gauss_points[p]
            wg = self.gauss_weights[p]
            Pe = self.pmatrix(xc, xg)
            fe += Ae * he / 3.0 * wg * np.dot(Pe.T, f[:num_dof_per_node])
        return fe

    def surface_force(self, xc, edge_no, q):
        num_nodes = self.num_nodes
        num_dof_per_node = self.num_dof_per_node
        fe = np.zeros(num_nodes * num_dof_per_node)
        edge = self.edges[edge_no]
        xd = xc[edge]
        for p in range(len(self.edge.gauss_points)):
            xg = self.edge.gauss_points[p]
            wg = self.edge.gauss_weights[p]
            N = self.edge.shape(xd, xg)
            dNds = self.edge.shapeder(xd, xg)
            dxds = np.dot(dNds, xd)
            Jd = np.sqrt(dxds[0] ** 2 + dxds[1] ** 2)
            for (i, ni) in enumerate(edge):
                for j in range(num_dof_per_node):
                    I = ni * num_dof_per_node + j
                    fe[I] += wg * Jd * N[i] * q[j]
        return fe
