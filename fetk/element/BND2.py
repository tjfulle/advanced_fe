import logging
import numpy as np

import fetk.dload
from .base import element, LINE, OFF, ON


class B1D2(element):
    """1D 2 node beam element"""

    signature = (LINE, 1, OFF, ON, OFF, OFF, OFF, ON, OFF, OFF, OFF, 2)
    properties = {"moment_of_inertia": {"aliases": None, "default": None}}

    def stiffness(self, xc, material, *args):
        """Computes the element stiffness for a 1D Euler beam

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates [[x1, y1, z1], [x2, y2, z2]]

        Returns
        -------
        K : ndarray
            Element stiffness stored as a symmetric matrix

        """
        v = xc[1] - xc[0]
        he = np.sqrt(np.dot(v, v))
        EI = material.E * self.moment_of_inertia
        ke = np.array(
            [
                [6.0, 3.0 * he, -6.0, 3.0 * he],
                [3.0 * he, 2.0 * he * he, -3.0 * he, he * he],
                [-6.0, -3.0 * he, 6.0, -3.0 * he],
                [3.0 * he, he * he, -3.0 * he, 2.0 * he * he],
            ]
        )
        return 2.0 * EI / he ** 3 * ke

    def force(self, xc, dload):
        num_nodes = self.num_nodes
        num_dof_per_node = self.num_dof_per_node
        fe = np.zeros(num_nodes * num_dof_per_node)
        if dload is None:
            return fe
        for (type, edge_no, f) in dload.items():
            if type == fetk.dload.surface_force:
                v = xc[1] - xc[0]
                he = np.sqrt(np.dot(v, v))
                fe[:] = f[1] * he / 12.0 * np.array([6.0, -he, 6.0, -he])
        return fe


class B2D2(element):
    """2D 2 node beam element"""

    nft = (LINE, 2, ON, ON, OFF, OFF, OFF, ON, OFF, OFF, OFF, 2)

    def set_properties(self, properties):
        """Set element properties

        Parameters
        ----------
        properties : dict
            required: properties["moment of inertia"]
            optional: properties["area"]

        """
        self.moment_of_inertia = properties.get("moment of inertia")
        if self.moment_of_inertia is None:
            raise ValueError("B1D2 element requires 'moment of inertia' property")
        self.area = properties.get("area")
        if self.area is None:
            logging.warn(
                "B2D2 element does not define 'area' property, using default=1"
            )
            self.area = 1.0

    def stiffness(self, xc, material, *args):
        """Computes the element stiffness for a 1D Euler beam

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates [[x1, y1, z1], ..., [xn, yn, zn]]

        Returns
        -------
        K : ndarray
            Element stiffness stored as a symmetric matrix

        """
        A, Izz = self.area, self.moment_of_inertia
        v = xc[1] - xc[0]
        he = np.sqrt(np.dot(v, v))
        n = v / he
        # Assemble element stiffness
        EA = material.E * A
        EI = material.E * Izz
        K1 = (
            EA
            / he
            * np.array(
                [
                    [1.0, 0.0, 0.0, -1.0, 0.0, 0.0],
                    [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                    [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                    [-1.0, 0.0, 0.0, 1.0, 0.0, 0.0],
                    [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                    [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                ]
            )
        )
        K2 = (
            2.0
            * EI
            / he ** 3
            * np.array(
                [
                    [0.0, 0.0, 0.0, 0.0, 0.0, 0],
                    [0.0, 6.0, 3.0 * he, 0.0, -6.0, 3.0 * he],
                    [0.0, 3.0 * he, 2.0 * he * he, 0.0, -3.0 * he, he * he],
                    [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                    [0.0, -6.0, -3.0 * he, 0.0, 6.0, -3.0 * he],
                    [0.0, 3.0 * he, he * he, 0.0, -3.0 * he, 2.0 * he * he],
                ]
            )
        )
        Te = np.eye(6)
        Te[0:2, 0:2] = Te[3:5, 3:5] = [[n[0], n[1]], [-n[1], n[0]]]
        return np.dot(np.dot(Te.T, K1 + K2), Te)
