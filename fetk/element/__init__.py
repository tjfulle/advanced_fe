from .base import LINE, PLANE, HEAT, SOLID
from .LND2 import L1D2, L2D2, L3D2
from .BND2 import B1D2, B2D2
from .C2D3 import C2D3
from .C3D4 import C3D4
from .C3D8F import C3D8F
from .C2D4S import C2D4S
from .C2D4E import C2D4E
from .C2D4EF import C2D4EF
from .C2D8E import C2D8E
from .CAX4 import CAX4
from .DC2D3 import DC2D3
from .DC2D4 import DC2D4
from . import block


_element_types = {
    "L1D2": L1D2,
    "L2D2": L2D2,
    "L3D2": L3D2,
    "B1D2": B1D2,
    "B2D2": B2D2,
    "C2D3": C2D3,
    "C3D4": C3D4,
    "C3D8F": C3D8F,
    "C2D4S": C2D4S,
    "C2D4E": C2D4E,
    "C2D8E": C2D8E,
    "C2D4EF": C2D4EF,
    "CAX4": CAX4,
    "DC2D3": DC2D3,
    "DC2D4": DC2D4,
}


def factory(name, **properties):
    type = _element_types.get(name.upper())
    if type is None:
        raise ValueError(f"Unknown element type {name}")
    return type(**properties)
