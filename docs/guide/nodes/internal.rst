=======================
Internal representation
=======================

Internally, nodes are represented by their coordinates ``coords`` and external to internal node number mapping ``node_map``.  ``coords`` and ``node_map`` should be constructed by ``fetk.node.fe_objects``.

---------------------
The coordinates array
---------------------

``coords`` is an array of nodal coordinates.  Nodes are numbered implicitly by the order n which they appear in ``coords``.  Nodes are numbered internally (beginning with 0) consecutively across ``coords``.

-------------------------------------
The external to internal node mapping
-------------------------------------

``node_map`` is a dictionary giving the internal node number ``n`` given an
external node number ``xn``

.. code-block:: python

    n = node_map[xn]

.. rubric:: Example

For the correspondence :eq:`ex_node_2`

.. code-block:: python

   node_map = {2: 0, 4: 1, 5: 2, 8: 3, 14: 4, 16: 5}

....................
Reverse node mapping
....................

``node_map`` can be searched in reverse to find the external node ``nx``
associated with internal node ``n``:

.. code-block:: python

    inverse_node_map = sorted(node_map.keys(), key=lambda k: node_map[k])

----------
Generating
----------

``coords`` and ``node_map`` are easily generated from the :ref:`nodes<defining_nodes>` table:

.. code-block:: python

    import numpy as np

    coords = np.array((len(nodes), 3))
    node_map = {}
    for (n, node) in enumerate(nodes):
        xn, *xc = node
        node_map[xn] = n
        coords[n, :len(xc)] = xc

``fetk.node.fe_objects`` implements the above algorithm with additional checks for correctness of input and can be used to generate ``coords`` and ``node_map`` from user defined :ref:`nodes<defining_nodes>`.

.......
Example
.......

.. code-block:: python

   >>> import fetk.node
   >>> nodes = [
       [1, 12., 12., 0.],
       [2, 12., 6., 0.],
       [3, 12., 0., 0.],
       [4, 18., 12., 0.],
       [5, 18., 6., 0.],
       [6, 18., 0., 0.],
       [7, 24., 12., 0.],
       [8, 24., 6., 0.],
       [9, 24., 0., 0.],
   ]
   >>> coords, node_map = fetk.node.fe_objects(nodes)
   >>> print(coords)
   array([[12., 12.,  0.],
       [12.,  6.,  0.],
       [12.,  0.,  0.],
       [18., 12.,  0.],
       [18.,  6.,  0.],
       [18.,  0.,  0.],
       [24., 12.,  0.],
       [24.,  6.,  0.],
       [24.,  0.,  0.]])
   >>> node_map
   {1: 0, 2: 1, 3: 2, 4: 3, 5: 4, 6: 5, 7: 6, 8: 7, 9: 8}
