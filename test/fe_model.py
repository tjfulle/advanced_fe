import numpy as np
import fetk.material
import fetk.element
from fetk.fe_model import fe_model
from fetk.util.filesystem import working_dir


def test_fe_model_plane_1(tmpdir):
    """
    \\\   \\\
   4 o-----o 3
     |\   /|
     | \ / |
     |--o--|
     | /5\ |
     |/   \|
   1 o-----o 2

    Nodes 3 and 4 are fixed.
    Force of -30kN applied in the y direction on node 5.
    """
    with working_dir(tmpdir):
        # Geometry
        nodes = [[1, 0, 0], [2, 0.4, 0], [3, 0.4, 0.4], [4, 0, 0.4], [5, 0.2, 0.2]]
        elements = [[1, 1, 2, 5], [2, 2, 3, 5], [3, 3, 4, 5], [4, 4, 1, 5]]
        thickness = 0.5e-2
        E, nu = 0.21e12, 0.3
        elastic = fetk.material.elastic({"E": E, "nu": nu})
        el = fetk.element.C2D3(thickness=thickness)
        model = fe_model("Test-plane", nodes, elements)
        model.element_block("B1", elements=[1, 2, 3, 4], material=elastic, element=el)
        model.boundary(3, "xy", 0.0)
        model.boundary(4, (0, 1), 0.0)
        model.cload(5, "y", -0.3e5)
        model.solve()
        expected_u = np.array(
            [
                [-0.165141e-5, -0.12504538e-4],
                [0.165141e-5, -0.12504535e-4],
                [0.0, 0.0],
                [0.0, 0.0],
                [0.0, -0.16279491e-4],
            ]
        )
        assert np.allclose(model.solution.u, expected_u)
